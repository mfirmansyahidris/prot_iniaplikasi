package fi.com.iniaplikasi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import fi.com.iniaplikasi.utils.DatabaseHandler;

public class IniActivity extends AppCompatActivity {
    DatabaseHandler db_handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ini);

        db_handler = new DatabaseHandler(this);
        if (db_handler.getUserCount() < 1) {
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }else{
            Intent intent = new Intent(getApplicationContext(), ListDosen.class);
            intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
