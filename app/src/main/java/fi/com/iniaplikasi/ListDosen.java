package fi.com.iniaplikasi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import fi.com.iniaplikasi.dialog.AddDosen;
import fi.com.iniaplikasi.dialog.AddDosen_i;
import fi.com.iniaplikasi.models.User;
import fi.com.iniaplikasi.models.gsonInterface.AllDosen;
import fi.com.iniaplikasi.services.ApiInterface;
import fi.com.iniaplikasi.services.ServiceGenerator;
import fi.com.iniaplikasi.utils.DatabaseHandler;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListDosen extends AppCompatActivity implements AddDosen_i, ListDosenAdapter_i{

    DatabaseHandler db_handler;
    User user;

    List<AllDosen.Result> listDosen = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListDosenAdapter dosenAdapter;

    FloatingActionButton fab_add_dosen;
    LinkedHashMap<String, String> dialog_lable = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_dosen);

        db_handler = new DatabaseHandler(this);
        user = db_handler.getUser();

        fab_add_dosen = findViewById(R.id.fab_add);
        recyclerView = findViewById(R.id.rv_list_dosen);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        ListDosenAdapter.setClickListener(ListDosen.this);

        fab_add_dosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_lable.clear();
                dialog_lable.put("title", "Tambahkan Dosen");
                dialog_lable.put("positive", "Tambahkan");
                dialog_lable.put("neutral", "Batalkan");
                AddDosen.setClickListener(ListDosen.this);
                final AddDosen addDosen = new AddDosen("insert", getFragmentManager(), ListDosen.this, dialog_lable);
                addDosen.showAlert();
            }
        });

        load_list();
    }

    @Override
    public void Dialog_onClickListener(LinkedHashMap<String, String> dosen_info) {
        if(dosen_info.get("action").equals("insert")){
            final ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class, user.getToken());
            LinkedHashMap<String, String> dosen_header = new LinkedHashMap<>();
            String token = "Bearer " + user.getToken();
            dosen_header.put("Authorization", token);
            dosen_header.put("dosen_name", dosen_info.get("nama"));
            dosen_header.put("dosen_status", dosen_info.get("status"));

            Log.d("anak_baik", token + " | " + dosen_info.get("nama") + " | " + dosen_info.get("status") + "|" +  dosen_info.get("action"));
            Call<ResponseBody> call = apiInterface.insertDosen(dosen_header);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    load_list();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }else if(dosen_info.get("action").equals("edit")){

        }else if(dosen_info.get("action").equals("delete")){

        }
    }

    @Override
    public void Adapter_onClickListener(String dosen_id, String dosen_nama, String dosen_status) {
        dialog_lable.clear();
        dialog_lable.put("title", "Ubah Dosen");
        dialog_lable.put("positive", "Ubah");
        dialog_lable.put("negative", "Hapus");
        dialog_lable.put("neutral", "Batalkan");
        dialog_lable.put("dosen_nama", dosen_nama);
        dialog_lable.put("dosen_status", dosen_status);
        AddDosen.setClickListener(ListDosen.this);
        final AddDosen addDosen = new AddDosen("edit", getFragmentManager(), ListDosen.this, dialog_lable);
        addDosen.showAlert();
    }

    private void load_list(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Memuat data ...");
        progressDialog.show();

        listDosen.clear();
        final ApiInterface apiInterface = ServiceGenerator.createService(ApiInterface.class, user.getToken());
        LinkedHashMap<String, String> dosen_list_header = new LinkedHashMap<>();
        String token = "Bearer " + user.getToken();
        dosen_list_header.put("Authorization", token);

        Call<AllDosen> call = apiInterface.getDosenAll(dosen_list_header);
        call.enqueue(new Callback<AllDosen>() {
            @Override
            public void onResponse(Call<AllDosen> call, Response<AllDosen> response) {
                progressDialog.dismiss();
                if (response.code() == 200) {
                    listDosen = response.body().getResults();

                    dosenAdapter = new ListDosenAdapter(listDosen);
                    recyclerView.setAdapter(dosenAdapter);
                    dosenAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllDosen> call, Throwable t) {
                progressDialog.dismiss();
                final AlertDialog alertDialog = new AlertDialog.Builder(ListDosen.this).create();
                alertDialog.setMessage("Tidak dapat menghubungkan server.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Oke", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }
}
