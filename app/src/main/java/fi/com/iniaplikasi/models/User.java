package fi.com.iniaplikasi.models;

/**
 * Created by _fi on 5/28/2018.
 */
public class User {
    public String username;
    public String domain;
    public String token;

    //empty constructor
    public User(){}

    //constructor
    public User(String username, String domain, String token){
        this.username = username;
        this.domain = domain;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
