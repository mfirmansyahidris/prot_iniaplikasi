package fi.com.iniaplikasi.models.gsonInterface;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by _fi on 5/28/2018.
 */
public class AllDosen {
    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private List<Result> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public class Result{
        @SerializedName("dosen_id")
        private String dosen_id;

        @SerializedName("dosen_name")
        private String dosen_name;

        @SerializedName("dosen_status")
        private String dosen_status;

        @SerializedName("update_at")
        private String update_at;

        @SerializedName("create_at")
        private String create_at;

        public String getDosen_id() {
            return dosen_id;
        }

        public void setDosen_id(String dosen_id) {
            this.dosen_id = dosen_id;
        }

        public String getDosen_name() {
            return dosen_name;
        }

        public void setDosen_name(String dosen_name) {
            this.dosen_name = dosen_name;
        }

        public String getDosen_status() {
            return dosen_status;
        }

        public void setDosen_status(String dosen_status) {
            this.dosen_status = dosen_status;
        }

        public String getUpdate_at() {
            return update_at;
        }

        public void setUpdate_at(String update_at) {
            this.update_at = update_at;
        }

        public String getCreate_at() {
            return create_at;
        }

        public void setCreate_at(String create_at) {
            this.create_at = create_at;
        }
    }
}
