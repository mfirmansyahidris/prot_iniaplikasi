package fi.com.iniaplikasi.models.gsonInterface;

import com.google.gson.annotations.SerializedName;

/**
 * Created by _fi on 5/28/2018.
 */
public class Auth {
    @SerializedName("status")
    private String status;

    @SerializedName("domain")
    private String domain;

    @SerializedName("token")
    private String token;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
