package fi.com.iniaplikasi.services;

import java.util.LinkedHashMap;

import fi.com.iniaplikasi.models.gsonInterface.AllDosen;
import fi.com.iniaplikasi.models.gsonInterface.Auth;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * Created by _fi on 5/28/2018.
 */
public interface ApiInterface {
    @POST("auth/login")
    Call<Auth> getAuth(@HeaderMap LinkedHashMap<String,String> headers);

    @GET("dosen")
    Call<AllDosen> getDosenAll(@HeaderMap LinkedHashMap<String,String> headers);

    @POST("dosen")
    Call<ResponseBody> insertDosen(@HeaderMap LinkedHashMap<String, String> headers);
}
